import $RefParser from "@apidevtools/json-schema-ref-parser";
import clipboard from "clipboardy";
import traverse from "traverse";
import {
  ROME_NODE_DESCRIPTION,
  ROME_OUTPUT_FILE,
  ROME_SCHEMA_URL,
} from "./constants";
import { saveSchema } from "./utils";

const main = async (): Promise<void> => {
  // https://apitools.dev/json-schema-ref-parser/docs/ref-parser.html#dereferenceschema-options-callback
  // https://apitools.dev/json-schema-ref-parser/docs/ref-parser.html#bundleschema-options-callback
  // https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/json-schema/index.d.ts#L569
  const schema = await $RefParser.dereference(ROME_SCHEMA_URL);

  // const fullSchema = JSON.stringify(await $RefParser.bundle(schema), null, 2);
  const fullSchema = await $RefParser.bundle(schema);
  saveSchema(ROME_OUTPUT_FILE, fullSchema);

  // https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/json-schema/index.d.ts#L670
  // https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/traverse/index.d.ts#L107
  // https://stackoverflow.com/a/64226369
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Optional_chaining
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/flatMap#for_adding_and_removing_items_during_a_map
  const linter = traverse(fullSchema).get(["properties", "linter"]);
  const linterRules = traverse(linter).reduce(function (acc, node) {
    // const isCategoryNode =
    //   node &&
    //   Object.prototype.hasOwnProperty.call(node, "description") &&
    //   node.description === ROME_NODE_DESCRIPTION;
    const isCategoryNode = node?.description === ROME_NODE_DESCRIPTION;
    const categoryName = this.parents.at(-2)?.key;

    if (isCategoryNode && categoryName) {
      // console.log(typeof node, node);
      // console.log(this.parent);
      // console.log(this.keys);
      // console.log(this.path);
      // console.log(this.node);
      // console.log(this.key);
      // console.log(this.parents);

      // const categoryName = this.parents.at(-2)?.key!;
      // console.log(categoryName);

      const rules = Object.fromEntries(
        Object.keys(node.properties).flatMap((key) =>
          key !== "recommended" ? [[key, "error"]] : []
        )
      );
      // console.log(rules);

      acc[categoryName] = rules;

      // "Prevents traversing descendents of the current node."
      this.block();
      // "Stops traversal entirely."
      // this.stop();
    }

    return acc;
  }, {});
  // console.log(linterRules);

  // traverse(fullSchema).forEach(function (x) {
  //   if (this.key === "linter") {
  //     console.log(traverse(x).paths());
  //   }
  // });

  const romeConfig = {
    linter: {
      enabled: true,
      rules: linterRules,
    },
  };
  const romeConfigOutput = JSON.stringify(romeConfig, null, 2);
  console.log(romeConfigOutput);

  // https://www.npmjs.com/package/clipboardy/v/2.3.0
  // clipboard.writeSync(romeConfigOutput);
  await clipboard.write(romeConfigOutput);

  console.log("Copied to clipboard!");
};

main();

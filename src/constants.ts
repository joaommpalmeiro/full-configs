import { join } from "path";

export const OUTPUT_FOLDER = join(__dirname, "..", "output");
export const SCHEMA_OUTPUT_FOLDER = join(OUTPUT_FOLDER, "schemas");
export const ROME_OUTPUT_FILE = join(SCHEMA_OUTPUT_FOLDER, "rome.json");

export const ROME_SCHEMA_URL =
  "https://raw.githubusercontent.com/rome/tools/cli/v11.0.0/npm/rome/configuration_schema.json";
export const ROME_NODE_DESCRIPTION =
  "A list of rules that belong to this group";

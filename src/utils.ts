import type { JSONSchema } from "@apidevtools/json-schema-ref-parser/dist/lib/types";
import { ensureDir, pathExists, writeJson } from "fs-extra";
import { SCHEMA_OUTPUT_FOLDER } from "./constants";

// https://github.com/jprichardson/node-fs-extra#sync-vs-async-vs-asyncawait
// https://github.com/jprichardson/node-fs-extra/blob/master/docs/writeJson.md
// https://github.com/APIDevTools/json-schema-ref-parser/blob/v10.1.0/package.json#L44
// https://github.com/APIDevTools/json-schema-ref-parser/blob/v10.1.0/lib/types/index.ts
// https://github.com/jprichardson/node-fs-extra/blob/master/docs/outputJson.md
// https://twitter.com/DavidKPiano/status/1561082799515262976
// https://nodejs.org/api/fs.html#fs-constants
export async function saveSchema(
  outputPath: string,
  schema: JSONSchema,
  overwrite: boolean = false
): Promise<void> {
  try {
    await ensureDir(SCHEMA_OUTPUT_FOLDER);
    const outputExists = await pathExists(outputPath);

    if (!outputExists || overwrite) {
      await writeJson(outputPath, schema, { spaces: 2 });
    }
  } catch (err) {
    console.error(err);
  }
}

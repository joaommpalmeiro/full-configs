# full-configs

## Development

```bash
nvm install && nvm use && node --version
```

or

```bash
nvm use && node --version
```

```bash
npm install
```

```bash
npm run dev:rome
```

## Notes

- `npm install @apidevtools/json-schema-ref-parser fs-extra traverse clipboardy@2.3.0 && npm install -D typescript ts-node @tsconfig/node18 @types/fs-extra @types/traverse`
- https://docs.rome.tools/lint/rules/
- https://www.npmjs.com/package/@apidevtools/json-schema-ref-parser
- https://www.npmjs.com/package/undici
- https://puruvj.dev/blog/create-folder-if-not-exists
- https://www.npmjs.com/package/obj-walker
